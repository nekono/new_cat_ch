﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CatCh.System;

namespace CatCh.Title
{
    public class TitleScene : MonoBehaviour
    {
        // Update is called once per frame
        void Update()
        {
            if (InputManager.Instance.GetAnyKey())
            {
                SceneLoader.Instance.LoadScene(SceneName.Select);
            }
        }
    }
}
