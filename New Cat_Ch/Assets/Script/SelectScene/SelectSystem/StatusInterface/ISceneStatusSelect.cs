﻿
//選曲シーン内のそれぞれの状態
public interface ISceneStatusSelect : ISceneStatus
{
    void ChangeSceneStatus(SelectSceneStatus.MusicSelectStatus musicSelectStatus);
}
