﻿
//選曲シーンの該当状態での処理
public interface ISceneStatus
{
    void Process(SelectSceneStatus.MusicSelectStatus musicSelectStatus);
}