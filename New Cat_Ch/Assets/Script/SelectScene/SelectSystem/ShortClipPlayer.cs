﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShortClipPlayer : MonoBehaviour
{
    [SerializeField] private AudioSource audioSource = null;

    public void PlayShortClip(AudioClip shortClip)
    {
        audioSource.clip = shortClip;
        audioSource.Play();
    }
}
