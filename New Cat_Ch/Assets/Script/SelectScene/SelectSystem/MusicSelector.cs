﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 選曲画面での曲番号変更
/// </summary>

public class MusicSelector : MonoBehaviour
{
    private int selectNo = 0;
    private int musicsValue = 0;

    InputManager.SetButtonList leftButton = InputManager.SetButtonList.s;
    InputManager.SetButtonList rightButton = InputManager.SetButtonList.l;


    void Start()
    {
        musicsValue = MusicDataManager.Instance.GetMusicListCount();
    }

    void Update()
    {
        MusicNoSelector();
    }
    
    //曲を選択するNoを変える
    private void MusicNoSelector()
    {
        if (InputManager.Instance.GetDownControllerKey(leftButton.ToString())) { selectNo -= 1; SoundEffects.Instance.PlayEnterSE(); }
        if (InputManager.Instance.GetDownControllerKey(rightButton.ToString())) { selectNo += 1; SoundEffects.Instance.PlayEnterSE(); }
        if (selectNo >= musicsValue) { selectNo = 0; }
        if (selectNo < 0) { selectNo = musicsValue - 1; }
    }

    public int GetSelectNo()
    {
        return selectNo;
    }
}
