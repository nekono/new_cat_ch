﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//タイル(ジャケット)のアニメーション状態管理
public class TileAnimationManager : MonoBehaviour, ISceneStatus
{
    [SerializeField] private TileAnimation[] tileAnimation = null;
    [SerializeField] private JacketSetter jacketSetter = null;
    [SerializeField] private SelectMusicInfoUI selectMusicInfoUI = null;

    InputManager.SetButtonList leftButton = InputManager.SetButtonList.l;
    InputManager.SetButtonList rightButton = InputManager.SetButtonList.s;

    [SerializeField] private SelectSceneStatus.MusicSelectStatus myStateName;

    private int moveDir = 0;
    private float moveRate = 0.0f;
    private bool isMoving = false;
    private const float moveSpeed = 10.0f;
    private float moveStartTime = 0.0f;


    private void Start()
    {
        EndTileAnimation();
    }

    //アニメーション処理
    public void Process(SelectSceneStatus.MusicSelectStatus musicSelectStatus)
    {
        if (musicSelectStatus == myStateName)
        {
            if (InputManager.Instance.GetAnyKey() && isMoving == false)
            {
                if (SelectSceneStatus.Instance.GetSelectSceneStatus() == SelectSceneStatus.MusicSelectStatus.MusicSelect)
                {
                    if (InputManager.Instance.GetDownControllerKey(leftButton.ToString())) { moveDir = -1; }
                    if (InputManager.Instance.GetDownControllerKey(rightButton.ToString())) { moveDir = 1; }
                }

                if (moveDir != 0)
                {
                    moveStartTime = Time.time;
                    isMoving = true;
                }
            }

            if (isMoving)
            {
                moveRate = (Time.time - moveStartTime) * moveSpeed;
                foreach (var tile in tileAnimation) { tile.TileMoveing(moveStartTime, moveRate, moveDir); }
            }

            //移動終了
            if (moveRate >= 1.0f)
            {
                moveRate = 0.0f;
                moveDir = 0;
                isMoving = false;
                EndTileAnimation();
            }
        }
    }

    //アニメーション終了を通知
    private void EndTileAnimation()
    {
        jacketSetter.UpdateJacket();
        selectMusicInfoUI.UpdateMusicInfo();
    }
}
