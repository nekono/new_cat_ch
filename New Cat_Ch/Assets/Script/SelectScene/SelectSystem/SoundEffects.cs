﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//効果音
public class SoundEffects : SingletonMonoBehaviour<SoundEffects>
{
    [SerializeField] private AudioClip enterSE;
    [SerializeField] private AudioClip cancelSE;
    [SerializeField] private AudioSource audioSource;

    public void PlayEnterSE()
    {
        audioSource.PlayOneShot(enterSE);
    }

    public void PlayCancelSE()
    {
        audioSource.PlayOneShot(cancelSE);
    }
}
