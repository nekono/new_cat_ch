﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// スピード設定・確認画面およびプレイ開始決定画面
/// </summary>

public class SettingSelectStatus : MonoBehaviour, ISceneStatusSelect
{
    [SerializeField] private GameObject stateCanves = null;
    [SerializeField] private SelectSceneStatus.MusicSelectStatus myStateName;

    [SerializeField] private RectTransform noteSpeedPreviewPanel = null;
    [SerializeField] private RectTransform speedSettingPanel = null;

    [SerializeField] private Vector2 previewPanelInitPos = new Vector2(0.0f, 0.0f);
    [SerializeField] private Vector2 speedSetingPanelInitPos = new Vector2(0.0f, 0.0f);

    [SerializeField] private Vector2 previewPanelPurposePos = new Vector2(0.0f, 0.0f);
    [SerializeField] private Vector2 speedSetingPanelPurposePos = new Vector2(0.0f, 0.0f);

    private float moveRate = 0.0f;
    private const float moveSpeed = 8.0f;
    private float moveStartTime = 0.0f;

    public void ChangeSceneStatus(SelectSceneStatus.MusicSelectStatus musicSelectStatus)
    {
        if (musicSelectStatus == myStateName) {
            moveRate = 0.0f;
            moveStartTime = Time.time;
            stateCanves.SetActive(true);
        }
        else {
            moveRate = 0.0f;
            moveStartTime = Time.time;
        }
    }

    public void Process(SelectSceneStatus.MusicSelectStatus musicSelectStatus)
    {
        if (musicSelectStatus == myStateName)
        {
            moveRate = (Time.time - moveStartTime) * moveSpeed;
            noteSpeedPreviewPanel.anchoredPosition = Vector2.Lerp(previewPanelInitPos, previewPanelPurposePos, moveRate);
            speedSettingPanel.anchoredPosition = Vector2.Lerp(speedSetingPanelInitPos, speedSetingPanelPurposePos, moveRate);
        }
        if (musicSelectStatus != myStateName && moveRate <= 1.0f)
        {
            moveRate = (Time.time - moveStartTime) * moveSpeed;
            noteSpeedPreviewPanel.anchoredPosition = Vector2.Lerp(previewPanelPurposePos, previewPanelInitPos, moveRate);
            speedSettingPanel.anchoredPosition = Vector2.Lerp(speedSetingPanelPurposePos, speedSetingPanelInitPos, moveRate);
            if(moveRate >= 1.0f){ stateCanves.SetActive(false); }
        }
    }
}
