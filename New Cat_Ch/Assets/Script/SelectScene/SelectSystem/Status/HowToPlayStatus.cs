﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 遊び方画面の表示
/// </summary>

public class HowToPlayStatus : MonoBehaviour, ISceneStatusSelect
{
    [SerializeField] private GameObject stateCanves = null;
    [SerializeField] private SelectSceneStatus.MusicSelectStatus myStateName;   //自身のシーン名

    [SerializeField] private RectTransform manualPanel = null;

    [SerializeField] private Vector2 manualPanelInitPos = new Vector2(0.0f, 0.0f);
    [SerializeField] private Vector2 manualPanelPurposePos = new Vector2(0.0f, 0.0f);

    private float moveRate = 0.0f;
    private const float moveSpeed = 8.0f;
    private float moveStartTime = 0.0f;

    public void ChangeSceneStatus(SelectSceneStatus.MusicSelectStatus musicSelectStatus)
    {
        if (musicSelectStatus == myStateName)
        {
            moveRate = 0.0f;
            moveStartTime = Time.time;
            stateCanves.SetActive(true);
        }
        else
        {
            moveRate = 0.0f;
            moveStartTime = Time.time;
        }
    }

    public void Process(SelectSceneStatus.MusicSelectStatus musicSelectStatus)
    {

        if (musicSelectStatus == myStateName)
        {
            moveRate = (Time.time - moveStartTime) * moveSpeed;
            manualPanel.anchoredPosition = Vector2.Lerp(manualPanelInitPos, manualPanelPurposePos, moveRate);
        }
        if (musicSelectStatus != myStateName && moveRate <= 1.0f)
        {
            moveRate = (Time.time - moveStartTime) * moveSpeed;
            manualPanel.anchoredPosition = Vector2.Lerp(manualPanelPurposePos, manualPanelInitPos, moveRate);
            if (moveRate >= 1.0f) { stateCanves.SetActive(false); }
        }
    }
}
