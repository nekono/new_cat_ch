﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 選択画面のシーン状態管理
/// </summary>

public class SelectSceneStatus : SingletonMonoBehaviour<SelectSceneStatus>
{
    [SerializeField] private GameObject[] sceneStateCanvas;
    [SerializeField] private List<ISceneStatusSelect> scenesList = new List<ISceneStatusSelect>();
    private List<ISceneStatus> processingScenesList = new List<ISceneStatus>();

    InputManager.SetButtonList cancelButton = InputManager.SetButtonList.f;
    InputManager.SetButtonList enterButton = InputManager.SetButtonList.j;
    
    //選択画面シーン内の子シーン(曲選択、設定画面など)
    public enum MusicSelectStatus
    {
        MusicSelect,
        SettingSelect,
        HowToPlay
    }

    //選曲シーンのシーンステータス
    private MusicSelectStatus musicSelectStatus = MusicSelectStatus.MusicSelect;

    private void Start()
    {
        foreach (var canvas in sceneStateCanvas)
        {
            var sceneCompornent = canvas.gameObject.GetComponent<ISceneStatus>();
            if (sceneCompornent != null)
            {
                processingScenesList.Add(sceneCompornent);

                var scene = sceneCompornent as ISceneStatusSelect;
                if (scene != null)
                {
                    scenesList.Add(scene);
                }
            }
        }
    }

    private void Update()
    {
        switch (musicSelectStatus)
        {
            ///選曲シーン
            case MusicSelectStatus.MusicSelect:
                //ノーツ速度設定およびプレイ決定画面へ
                if (InputManager.Instance.GetDownControllerKey(enterButton.ToString())){
                    SetSelectSceneStatus(MusicSelectStatus.SettingSelect);
                    SoundEffects.Instance.PlayEnterSE();
                }
                //遊び方表示へ
                if (InputManager.Instance.GetDownControllerKey(cancelButton.ToString()))
                {
                    SetSelectSceneStatus(MusicSelectStatus.HowToPlay);
                    SoundEffects.Instance.PlayEnterSE();
                }
                break;

            ///速度設定およびプレイ決定シーン
            case MusicSelectStatus.SettingSelect:
                //プレイ開始
                if (InputManager.Instance.GetDownControllerKey(enterButton.ToString())){
                    GameSceneLoader.Instance.LoadScene();
                    SoundEffects.Instance.PlayEnterSE();
                }
                //楽曲選択画面へ戻る
                if (InputManager.Instance.GetDownControllerKey(cancelButton.ToString())) {
                    SetSelectSceneStatus(MusicSelectStatus.MusicSelect);
                    SoundEffects.Instance.PlayCancelSE();
                }
                break;

            ///遊び方画面
            case MusicSelectStatus.HowToPlay:
                //楽曲選択画面へ戻る
                if (InputManager.Instance.GetDownControllerKey(cancelButton.ToString()))
                {
                    SetSelectSceneStatus(MusicSelectStatus.MusicSelect);
                    SoundEffects.Instance.PlayCancelSE();
                }
                break;

            default:
                break;
        }

        foreach (var scene in processingScenesList)
        {
            scene.Process(musicSelectStatus);
        }
    }

       
    //シーンの画面を切り替える
    private void SelectSceneViewSwitch()
    {
        foreach (var canvas in scenesList)
        {
            canvas.ChangeSceneStatus(musicSelectStatus);
        }
    }

    //選曲シーンのシーンステータスを設定
    public void SetSelectSceneStatus(MusicSelectStatus newStatus)
    {
        if (musicSelectStatus != newStatus)
        {
            musicSelectStatus = newStatus;
            SelectSceneViewSwitch();
        }
    }

    //選曲シーンのシーンステータスを取得
    public MusicSelectStatus GetSelectSceneStatus()
    {
        return musicSelectStatus;
    }
}
