﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


//選曲・プレイ時に必要な情報保持
public class GameSceneLoader : SingletonMonoBehaviour<GameSceneLoader>
{
    private bool isPlayMode = false;   //オートモードで再生

    [SerializeField] private string musicName = "HEILIG";         //曲名
    [SerializeField] private int difficulty = 0;                   //難易度(0,1,2)
    [SerializeField] private float noteSpeed = 10.0f;             //ノーツスピード
    [SerializeField] private bool playAutoMode = false;           //オートモード

    [SerializeField] private Image loadingImage = null;           //シーン切替時表示Image
    private bool isLoadingProcess = false;                        //シーン切替処理稼働中か
    private const float gameStartWaitTime = 1.0f;                 //シーン切替後の待機時間秒

    private AsyncOperation loadAsync;
    private GameManager gameSceneGameManager = null;               //Gameシーンのゲームマネージャー

    public void SetMusicName(string name){ musicName = name; }
    public void SetDifficulty(int difficultyValue) { difficulty = difficultyValue; }
    public float NoteSpeed { get { return noteSpeed; } set { noteSpeed = value; } }
    public void SetPlayMode(bool setPlayAutoMode) { playAutoMode = setPlayAutoMode; }


    //ゲームシーンを読み込む
    public void LoadScene()
    {
        if (isLoadingProcess == false)
        {
            isLoadingProcess = true;
            DontDestroyOnLoad(gameObject);
            SceneManager.sceneLoaded += SendPlayData;
            loadAsync = SceneManager.LoadSceneAsync("Game");
            loadAsync.allowSceneActivation = false;
            StartCoroutine("FadeOut");
        }
    }


    //プレイする曲などの情報を次のシーンへ送る
    private void SendPlayData(Scene next, LoadSceneMode mode)
    {
        gameSceneGameManager = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
        gameSceneGameManager.isAutoMode = isPlayMode;         //オートプレイモード設定
        gameSceneGameManager.musicName = musicName;           //楽曲名
        gameSceneGameManager.noteSpeed = noteSpeed;           //スピード
        gameSceneGameManager.difficulty = difficulty;           //難易度
        gameSceneGameManager.GameInit();

        StartCoroutine("FadeIn");
        SceneManager.sceneLoaded -= SendPlayData;
    }


    IEnumerator FadeOut()
    {
        float alpha = 0.0f;
        while (alpha < 1.0f)
        {
            alpha += 0.1f;
            loadingImage.color = new Color(1.0f, 1.0f, 1.0f, alpha);
            yield return new WaitForSeconds(0.05f);
        }
        loadAsync.allowSceneActivation = true;
    }

    IEnumerator FadeIn()
    {
        yield return new WaitForSeconds(0.5f);
        float alpha = 1.0f;
        while (alpha > 0.0f)
        {
            alpha -= 0.1f;
            loadingImage.color = new Color(1.0f, 1.0f, 1.0f, alpha);
            yield return new WaitForSeconds(0.05f);
        }
        StartCoroutine("GameStartWait");
    }

    IEnumerator GameStartWait()
    {
        yield return new WaitForSeconds(gameStartWaitTime);
        gameSceneGameManager.StartGame();
        Destroy(gameObject);
    }
}
