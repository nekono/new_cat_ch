﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//タイル(ジャケット絵)のアニメーション
public class TileAnimation : MonoBehaviour
{
    //表示タイルの位置属性
    private enum TileAnimKind
    {
        OutEndLeft,
        EndLeft,
        LeftTile,
        CenterLeftTile,
        CenterTile,
        CenterRightTile,
        RightTile,
        EndRight,
        OutEndRight
    }

    [SerializeField] private TileAnimKind tileAnimKind;
    [SerializeField] private TileAnimationData tileAnimData;

    [SerializeField]
    private RectTransform rectTransform;

    public void TileMoveing(float startTime, float moveRate, int moveDir)
    {
        //座標・スケールを変更
        float scale = Mathf.Lerp(rectTransform.localScale.x, tileAnimData.AnimeData[(int)tileAnimKind + moveDir].scale, moveRate);
        rectTransform.localPosition = Vector2.Lerp(rectTransform.localPosition, tileAnimData.AnimeData[(int)tileAnimKind + moveDir].position, moveRate);
        rectTransform.localScale = new Vector2(scale, scale);

        //移動終了
        if (moveRate >= 1.0f)
        {
            rectTransform.localPosition = tileAnimData.AnimeData[(int)tileAnimKind].position;
            scale = tileAnimData.AnimeData[(int)tileAnimKind].scale;
            rectTransform.localScale = new Vector2(scale, scale);
        }
    }
}
