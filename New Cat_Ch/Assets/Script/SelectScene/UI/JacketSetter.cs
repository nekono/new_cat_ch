﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class JacketSetter : MonoBehaviour
{
    [SerializeField] private Image[] tileImage;
    [SerializeField] private List<MusicData> musicDataLists;
    [SerializeField] MusicSelector musicSelector = null;

    private int selectNo = 0;
    private int musicValue = 0;

    private void Start()
    {
        musicDataLists = MusicDataManager.Instance.GetMusicList();
        musicValue = MusicDataManager.Instance.GetMusicListCount();
    }

    //ジャケット絵を描画(再設置)更新
    private void LateUpdate()
    {

        //****もっといい実装にするべき(思いつかなかった)****
        //中央除き左側のジャケット
        int setNo = selectNo;
        for (int tileIndex = 2; tileIndex >= 0; tileIndex--)
        {
            setNo--;
            if (setNo < 0) { setNo = musicValue - 1; }
            //tileImage[tileIndex].sprite = musicDataLists[setNo].GetJacket();
            tileImage[tileIndex].sprite = MusicDataManager.Instance.GetMusicList()[setNo].GetJacket();
        }

        //中央から右側のジャケット
        setNo = selectNo - 1;
        for (int tileIndex = 3; tileIndex < tileImage.Length; tileIndex++)
        {
            setNo++;
            if (setNo >= musicValue) { setNo = 0; }
            //tileImage[tileIndex].sprite = musicDataLists[setNo].GetJacket();
            tileImage[tileIndex].sprite = MusicDataManager.Instance.GetMusicList()[setNo].GetJacket();
        }
    }

    //選曲Noの変化をTileアニメ後に受け取る
    public void UpdateJacket()
    {
        selectNo = musicSelector.GetSelectNo();
    }
}
