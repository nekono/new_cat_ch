﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// 曲のデータリスト
/// </summary>

[SerializeField]
[CreateAssetMenu(fileName = "TileAnimationData", menuName = "CreateTileAnimationData")]
public class TileAnimationData : ScriptableObject
{
    [SerializeField] private TileAnimeData outEndLeftPos;
    [SerializeField] private TileAnimeData endLeftPos;
    [SerializeField] private TileAnimeData leftPos;
    [SerializeField] private TileAnimeData centerLeftPos;
    [SerializeField] private TileAnimeData centerPos;
    [SerializeField] private TileAnimeData centerRightPos;
    [SerializeField] private TileAnimeData rightPos;
    [SerializeField] private TileAnimeData endRightPos;
    [SerializeField] private TileAnimeData outEndRightPos;

    public TileAnimeData[] AnimeData => new[] { outEndLeftPos, endLeftPos, leftPos, centerLeftPos, centerPos, centerRightPos, rightPos, endRightPos, outEndRightPos };
}

[Serializable]
public struct TileAnimeData
{
    public Vector2 position;
    public float scale;
}
