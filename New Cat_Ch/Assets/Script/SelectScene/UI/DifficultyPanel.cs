﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DifficultyPanel : MonoBehaviour, ISceneStatus
{
    InputManager.SetButtonList leftButton = InputManager.SetButtonList.d;
    InputManager.SetButtonList rightButton = InputManager.SetButtonList.k;

    private bool isMoving = false;
    private int difficultyValue = 0;
    private float moveStartTime = 0.0f;
    private float moveRate = 0.0f;
    private const float moveDistance = 500.0f;
    private const float moveSpeed = 9.0f;

    [SerializeField] private RectTransform difficultyPanels = null;
    [SerializeField] private SelectSceneStatus.MusicSelectStatus myStateName;

    private void ChangeDifficulty()
    {
        if (difficultyValue <= 0) { difficultyValue = 0; }
        if (difficultyValue > 2) { difficultyValue = 2; }
        GameSceneLoader.Instance.SetDifficulty(difficultyValue);
        moveStartTime = Time.time;
        isMoving = true;
    }

    public void Process(SelectSceneStatus.MusicSelectStatus musicSelectStatus)
    {
        if (musicSelectStatus == myStateName)
        {
            if (InputManager.Instance.GetAnyKey() && isMoving == false)
            {
                if (InputManager.Instance.GetDownControllerKey(leftButton.ToString())) { difficultyValue--; ChangeDifficulty(); SoundEffects.Instance.PlayCancelSE(); }
                if (InputManager.Instance.GetDownControllerKey(rightButton.ToString())) { difficultyValue++; ChangeDifficulty(); SoundEffects.Instance.PlayEnterSE(); }
            }

            if (isMoving)
            {
                moveRate = (Time.time - moveStartTime) * moveSpeed;
                difficultyPanels.transform.localPosition = Vector2.Lerp(difficultyPanels.transform.localPosition, new Vector2(-difficultyValue * moveDistance, difficultyPanels.transform.localPosition.y), moveRate);
            }
            if (moveRate >= 1.0f)
            {
                isMoving = false;
            }
        }
    }
}
