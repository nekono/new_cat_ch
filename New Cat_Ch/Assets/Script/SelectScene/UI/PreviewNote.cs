﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ノーツのスピードプレビュー
/// </summary>

public class PreviewNote : MonoBehaviour, ISceneStatusSelect
{
    [SerializeField] private SelectSceneStatus.MusicSelectStatus myStateName;
    [SerializeField] private GameObject noteObject = null;
    [SerializeField] private float previewTime = 0.0f;

    private const float resetZPos = -12.0f;
    private float startTime = 0.0f;


    public void ChangeSceneStatus(SelectSceneStatus.MusicSelectStatus musicSelectStatus)
    {
        if(musicSelectStatus == myStateName)
        {
            startTime = Time.time + 3.0f;
            previewTime = Time.time;
        }
    }


    public void Process(SelectSceneStatus.MusicSelectStatus musicSelectStatus)
    {
        if (musicSelectStatus == myStateName)
        {
            previewTime += Time.deltaTime;

            float z = Note.defaultOffsetZ + (startTime - previewTime) * GameSceneLoader.Instance.NoteSpeed;

            Vector3 pos = new Vector3(0.0f, noteObject.transform.position.y, z);
            noteObject.transform.position = pos;

            if (z < resetZPos)
            {
                startTime = Time.time + 3.0f;
                previewTime = Time.time;
            }
        }
    }
}
