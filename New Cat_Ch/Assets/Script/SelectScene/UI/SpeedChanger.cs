﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


//スピード変更
public class SpeedChanger : MonoBehaviour, ISceneStatus
{
    [SerializeField] private Text speedValueText = null;
    [SerializeField] private SelectSceneStatus.MusicSelectStatus myStateName;

    InputManager.SetButtonList speedDownButton = InputManager.SetButtonList.d;
    InputManager.SetButtonList speedUpButton = InputManager.SetButtonList.k;
    InputManager.SetButtonList speedMoreDownButton = InputManager.SetButtonList.s;
    InputManager.SetButtonList speedMoreUpButton = InputManager.SetButtonList.l;

    private const float minSpeed = 5.0f;
    private const float maxSpeed = 50.0f;

    public void Process(SelectSceneStatus.MusicSelectStatus musicSelectStatus)
    {
        if (musicSelectStatus == myStateName)
        {
            var speed = GameSceneLoader.Instance.NoteSpeed;

            if (InputManager.Instance.GetDownControllerKey(speedDownButton.ToString())) { speed -= 0.5f; SoundEffects.Instance.PlayCancelSE(); }
            if (InputManager.Instance.GetDownControllerKey(speedUpButton.ToString())) { speed += 0.5f; SoundEffects.Instance.PlayEnterSE(); }
            if (InputManager.Instance.GetDownControllerKey(speedMoreDownButton.ToString())) { speed -= 5.0f; SoundEffects.Instance.PlayCancelSE(); }
            if (InputManager.Instance.GetDownControllerKey(speedMoreUpButton.ToString())) { speed += 5.0f; SoundEffects.Instance.PlayEnterSE(); }
            if (speed < minSpeed) { speed = minSpeed; }
            if (speed > maxSpeed) { speed = maxSpeed; }
            GameSceneLoader.Instance.NoteSpeed = speed;
            speedValueText.text = string.Format("{0:00.0}", speed);
        }
    }
}
