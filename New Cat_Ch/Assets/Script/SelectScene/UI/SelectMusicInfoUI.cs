﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectMusicInfoUI : MonoBehaviour
{

    [SerializeField] private TextFlow textFlow = null;
    [SerializeField] private MusicSelector musicSelector = null;
    [SerializeField] private Text musicInfoText = null;
    [SerializeField] private ShortClipPlayer shortClipPlayer = null;

    private int selectNo = 0;


    public void UpdateMusicInfo()
    {
        selectNo = musicSelector.GetSelectNo();
        var musicData = MusicDataManager.Instance.GetMusicList()[selectNo];
        string setTitle = musicData.GetMusicTitle();
        GameSceneLoader.Instance.SetMusicName(setTitle);
        var shortClip = musicData.GetShortClip();
        shortClipPlayer.PlayShortClip(shortClip);
        musicInfoText.text = string.Format(": {0}\n: {1}\n: {2}", musicData.GetArtist(), musicData.GetIllustrator(), musicData.GetNoteDesigner());
        textFlow.SetText(setTitle);
    }
}
