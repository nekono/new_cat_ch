﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ScoreManager : SingletonMonoBehaviour<ScoreManager>
{
    public double score { get; private set; } = 0.0f;
    public double viewScore { get; private set; } = 0.0f;
    private double oneScore = 0.0f;
    private int notesValue = 0;

    //スコア表示UI
    public Text scoreText;

    //判定用の時間(秒)
    public const float goodJudge = 0.20f;
    public const float greatJudge = 0.09f;
    public const float parfectJudge = 0.03f;
    public const float MAX_SCORE = 1000000.0f;     //スコア最大値


    //スコアによるランク基準値
    public const float RANK_NORMAL = 200000.0f;
    public const float RANK_BRONZE = 500000.0f;
    public const float RANK_SILVER = 700000.0f;
    public const float RANK_GOLD = 900000.0f;

    //ノーツの数を足していく(ノーツの総数)
    public void SetNotesValue(int value)
    {
        notesValue = value;
    }

    //100満点固定なので1ノーツ当たりの点数を出す
    public void CompleteCountNotes()
    {
        oneScore = 1000000.0f / notesValue;
    }

    //ノーツが自分の時間と曲の時間の差をここに渡してスコアを得る
    public void JudgeScore(float pushedTime)
    {
        //判定パーフェクト
        if (pushedTime < parfectJudge)
        {
            score += oneScore;
            NoteSoundEffectManager.Instance.PlaySoundEffect();
        }
        //判定グレート
        else if(pushedTime < greatJudge)
        {
            score += oneScore / 2;
            NoteSoundEffectManager.Instance.PlaySoundEffect();
        }
        //判定グッド
        else if (pushedTime < goodJudge)
        {
            score += oneScore / 4;
            NoteSoundEffectManager.Instance.PlaySoundEffect();
        }
    }

    //表示上のスコアの数値に動きを付ける
    private void Update()
    {
        if (score - viewScore >= 0.0f)
        {
            viewScore += (score - viewScore) / 2;
        }
        scoreText.text = string.Format("{0:0000000.00}", viewScore);
    }
}
