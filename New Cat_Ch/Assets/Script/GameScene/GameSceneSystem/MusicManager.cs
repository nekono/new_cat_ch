﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : SingletonMonoBehaviour<MusicManager>
{

    public AudioSource audioSource;
    public bool isPlay { get; private set; } = false;         //再生中か否か
    public bool isInitCount { get; private set; } = false;    //曲の始まる前に入れる余白時間が終わったか
    const float initTime = -5.5f;                             //曲の始まる前の余白時間(秒)
    public float playTime { get; private set; } = 0.0f;       //再生時間
    public bool isMusicEnd { get; private set; } = false;     //曲が終わったかどうか

    // Update is called once per frame
    void Update()
    {
        //曲の前に余白を入れる
        if(isPlay && !isInitCount)
        {
            playTime += Time.deltaTime;
            if(playTime >= 0)
            {
                isInitCount = true;
                audioSource.Play();
            }
        }

        //余白終了後は曲の再生中時間を反映
        if(isPlay && isInitCount){
            playTime = audioSource.time;
        }

        //曲の終了
        if(isInitCount && audioSource.isPlaying == false && isPlay)
        {
            isMusicEnd = true;
            isPlay = false;
            GameManager.Instance.GameOver();
        }

    }

  //曲を開始する
    public void StartMusic(AudioClip musicClip)
    {
        playTime = initTime;
        audioSource.clip = musicClip;
        isPlay = true;
    }
}
