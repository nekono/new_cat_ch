﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using CatCh.System;

public class GameManager : SingletonMonoBehaviour<GameManager>
{
    private Sprite jacket = null;
    public Image jacketUI;
    public TextFlow textFlow;

    public bool isAutoMode = false;

    public string musicName;            //曲名
    public int difficulty;              //難易度(0,1,2)
    public float noteSpeed = 10.0f;     //ノーツスピード

    public bool isGameOver { get; private set; } = false;   //ゲーム終了しているか

    private MusicData playMusicData = null;

    public void GameInit()
    {
        //曲データ取得
        playMusicData = MusicDataManager.Instance.SearchMusicData(musicName);

        //ノーツ準備
        NotesManager.Instance.NoteManagerInit(playMusicData.GetNoteData(difficulty));

        //ジャケット読み込み
        jacket = playMusicData.GetJacket();

        jacketUI.sprite = jacket;
        textFlow.SetText(musicName);
    }

    //ゲーム開始
    public void StartGame()
    {
        MusicManager.Instance.StartMusic(playMusicData.GetMusicClip());
        CenterTextUI.Instance.PlayAnim();
    }


    //楽曲選択画面に戻る
    public void ReturnMusicSelect()
    {
        SceneLoader.Instance.LoadScene(SceneName.Select);
    }

    //楽曲が終了
    public void GameOver()
    {
        StartCoroutine("MoveResultScene");
    }

    //ゲーム終了を知らせるポップアップとリザルトの表示
    IEnumerator MoveResultScene()
    {
        yield return new WaitForSeconds(1.0f);
        CenterTextUI.Instance.EndMusic();
        yield return new WaitForSeconds(5.0f);
        isGameOver = true;
    }

}
