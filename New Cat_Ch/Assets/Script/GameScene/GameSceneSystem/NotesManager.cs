﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using CatCh.System;


public class NotesManager : SingletonMonoBehaviour<NotesManager>
{
    [SerializeField] private Note notePrefab = null;
    private ObjectPool<Note> objectPool = null;
    private const float NotePutOutTime = 3.0f;                    //ノーツ出現時間
    private float[] laneNoteFirstTimes = { -1.0f, -1.0f, -1.0f, -1.0f };     //レーンごとの有効な一番先端のノーツの時間(時間が近いノーツが同時に消えることを防ぐため)
    private float[] deleteTimes = { 0, 0, 0, 0 };   //消えた瞬間のノーツ時間が入る(重複判定を避ける)
    private List<NoteData> noteDataList = new List<NoteData>();
    public float GetLaneNoteFirstTimes(NoteLane lane) { return laneNoteFirstTimes[(int)lane]; } //レーンに所属する先端のノーツ時間


    /// <summary>
    /// 譜面読み込み、オブジェクトプール生成
    /// </summary>
    public void NoteManagerInit(TextAsset csvText)
    {
        objectPool = new ObjectPool<Note>();
        objectPool.CreatePool(notePrefab, "NotePool");
        noteDataList = CsvLoader.CsvToNoteList(csvText);
        ScoreManager.Instance.CompleteCountNotes();                 //全ノーツの解析が終わったことを通知
    }

    private void Update()
    {
        //曲の再生中はノーツの動作と生成、無効化の監視
        if (MusicManager.Instance.isPlay){ NoteCheck(); }
    }


    //時間の監視とノーツ生成の判定
    void NoteCheck()
    {
        for (int i = 0; i < 4; i++) { deleteTimes[i] = -0.5f; }
     
        //生成時間になったノーツリスト化
        var createNotes = noteDataList
            .Where(x => x._active == false)
            .Where(x => x._time - NotePutOutTime < MusicManager.Instance.playTime)
            .ToList();
  
        if (createNotes != null)
        {
            foreach (var noteObj in createNotes)
            {
                var obj = objectPool.CreateObject();
                Create(obj, noteObj);
            }
        }
        createNotes.ForEach(toActive => toActive._active = true);
    }

    void Create(Note note,NoteData data)
    {
        //ノーツの初期化
        note.SetNoteData(data._lane, data._time, data._kind, data._length);
        note.SetLane();

        //同一レーン上での同時判定を避けるノーツの順序処理
        if (GetLaneNoteFirstTimes(data._lane) > data._time || GetLaneNoteFirstTimes(data._lane) == 0)
        {
            laneNoteFirstTimes[(int)data._lane - 1] = data._time;
        }
    }

    //ノーツ側からレーン番号と時間を送り、ここでレーンごとのアクティブな先頭のノーツを把握する
    public void NoteOrderControl(NoteLane lane, float time)
    {
        int laneNumber = (int)lane;
        if (deleteTimes[laneNumber] == -0.5f)
        {
            if (GetLaneNoteFirstTimes(lane) == -1.0f) { laneNoteFirstTimes[laneNumber] = time; }
            if (GetLaneNoteFirstTimes(lane) >= time) { laneNoteFirstTimes[laneNumber] = time; }
        }
    }

    //ノーツ側から判定されたことをレーン番号と時間を持って知らせる
    public void NoteJudged(NoteLane lane, float time)
    {
        int laneNumber = (int)lane;
        deleteTimes[laneNumber] = time;
        laneNoteFirstTimes[laneNumber] = -1.0f;
    }
}
