﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteSoundEffectManager : SingletonMonoBehaviour<NoteSoundEffectManager>
{
    [SerializeField] private AudioClip soundEffect = null;                 //効果音本体
    [SerializeField] private AudioSource audioSource = null;               //効果音

    //効果音を鳴らす
    public void PlaySoundEffect()
    {
        audioSource.PlayOneShot(soundEffect);
    }
}
