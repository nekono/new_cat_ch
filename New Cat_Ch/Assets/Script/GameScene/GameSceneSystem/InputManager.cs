﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


/// <summary>
/// 独自筐体およびキーボードの入力に対応した入力処理
/// </summary>

public class InputManager : SingletonMonoBehaviour<InputManager>
{
    //ボタン設定
    public enum SetButtonList
    {
        s,
        d,
        f,
        j,
        k,
        l
    }
    SetButtonList myLane;

    //ボタンが押されたとき
    public bool GetDownControllerKey(string key)
    {
        bool isControllerKey = Enum.TryParse(key, out SetButtonList keyNum);  //enumの文字列を数値に変換

        //列挙型内に文字列が存在した場合
        if (isControllerKey){
            if(Input.GetKeyDown("joystick button " + (int)keyNum))
            {
                return true;
            }
        }
        if (Input.GetKeyDown(key)) { return true; }
        return false;
    }

    //ボタンを押しているとき
    public bool GetControllerKey(string key)
    {
        bool isControllerKey = Enum.TryParse(key, out SetButtonList keyNum);  //enumの文字列を数値に変換

        //列挙型内に文字列が存在した場合
        if (isControllerKey)
        {
            if (Input.GetKey("joystick button " + (int)keyNum))
            {
                return true;
            }
        }
        if (Input.GetKey(key)) { return true; }
        return false;
    }

    //何かしらのボタンが押された
    public bool GetAnyKey()
    {
        if (Input.anyKeyDown)
        {
            return true;
        }
        return false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            GameClose();
        }
    }

    private void GameClose()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_STANDALONE
        UnityEngine.Application.Quit();
#endif
    }
}
