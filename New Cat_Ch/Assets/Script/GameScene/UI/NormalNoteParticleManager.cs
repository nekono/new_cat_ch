﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalNoteParticleManager : SingletonMonoBehaviour<NormalNoteParticleManager>
{
    [SerializeField] private NoteParticle[] noteParticle;

    public void PlayParticle(InputManager.SetButtonList laneKey)
    {
        foreach (var item in noteParticle)
        {
            item.PlayParticle(laneKey);
        }
    }

}
