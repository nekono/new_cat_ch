﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NikukyuuAnim : MonoBehaviour
{
    public SkinnedMeshRenderer skinnedMeshRenderer;
    public InputManager.SetButtonList setButton;

    KeyCode keyCode;
    private const float startAnimSpeed = 10.0f;
    private const float endAnimSpeed = 20.0f;

    //肉球ボタンアニメーション入力受付
    void Update()
    {
        if (InputManager.Instance.GetDownControllerKey(setButton.ToString()))
        {
            StartCoroutine("StartAnim");
        }
    }

    //肉球ボタンアニメーション
    IEnumerator StartAnim()
    {
        float animValue = 80;
        while (animValue < 100)
        {
            animValue+=startAnimSpeed;
            skinnedMeshRenderer.SetBlendShapeWeight(0, animValue);
            yield return new WaitForSeconds(0.0000001f);
        }
        while (animValue > 0)
        {
            animValue-=endAnimSpeed;
            skinnedMeshRenderer.SetBlendShapeWeight(0, animValue);
            yield return new WaitForSeconds(0.0000001f);
        }
        yield return null;
    }
}
