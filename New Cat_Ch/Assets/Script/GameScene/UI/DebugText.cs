﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class DebugText : MonoBehaviour
{
    //デバッグ用に表示するテキスト
    public Text text;

    // Update is called once per frame
    void Update()
    {
        text.text = string.Format("{0},{1}", MusicManager.Instance.audioSource.time, MusicManager.Instance.audioSource.clip.length);
        //text.text = Input.GetKeyDown(KeyCode.Joystick1Button0).ToString();
        //text.text = InputManager.Instance.GetDownControllerKey("c").ToString();
        //text.text = String.Format("{0:0000000.00}", MusicManager.Instance.playTime);
    }
}
