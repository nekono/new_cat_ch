﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CatCanManager : MonoBehaviour
{
    /// <summary>
    /// スコアに応じて出現する猫缶
    /// </summary>

    public Image normal;
    public Image bronze;
    public Image silver;
    public Image gold;
    private Color enableColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);

    void Update()
    {
        float scr = (float)ScoreManager.Instance.viewScore;
        if (ScoreManager.RANK_NORMAL <= scr) { normal.color = enableColor; }
        if (ScoreManager.RANK_BRONZE <= scr) { bronze.color = enableColor; }
        if (ScoreManager.RANK_SILVER <= scr) { silver.color = enableColor; }
        if (ScoreManager.RANK_GOLD <= scr) { gold.color = enableColor; }
    }
}
