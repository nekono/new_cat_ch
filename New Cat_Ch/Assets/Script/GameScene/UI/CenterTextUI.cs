﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CenterTextUI : SingletonMonoBehaviour<CenterTextUI>
{
    /// <summary>
    /// ゲーム開始、終了時の文字表示
    /// </summary>

    public Animator startAnim;
    [SerializeField] private Text centerText = null;

    public void PlayAnim()
    {
        startAnim.SetTrigger("playPopup");
    }

    public void EndMusic()
    {
        centerText.text = "FINISH";
        startAnim.SetTrigger("playPopup");
    }

    private void Start()
    {
        startAnim = GetComponent<Animator>();
    }
}
