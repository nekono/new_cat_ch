﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultUI : MonoBehaviour
{
    [SerializeField] private Text scoreText = null;
    [SerializeField] private Text gradeText = null;
    [SerializeField] private GameObject resultPanel = null;
    [SerializeField] private Sprite[] gradeImageSource = null;      //それぞれのランクの画像
    [SerializeField] private Image gradeImage = null;               //表示ランクイメージ
    [SerializeField] private AudioClip[] sound = null;              //効果音
    [SerializeField] private AudioSource soundEffect = null;
    [SerializeField] private GameObject pushAnyButtonText = null;   //結果発表後の表示オブジェクト

    private double resultScore = 0;                                 //スコア
    private int grade = 0;
    private string rankName = "FAILD...";                           //結果ランク名

    private float effectTime;                                       //結果発表の効果音
    private bool isResultEnd = false;

    //リザルト表示・有効化
    public void ToActiveResultUI()
    {
        resultPanel.SetActive(true);
    }

    //結果表示
    public void ShowResult()
    {
        if (ScoreManager.Instance.score - resultScore > 0.000001)
        {
            resultScore += (ScoreManager.Instance.score - resultScore) / 5;     //表示スコア加算
            effectTime += Time.deltaTime;                                       //効果音を鳴らす間

            if (effectTime >= 0.05f)
            {
                soundEffect.PlayOneShot(sound[0]);  //ドラムロール
                effectTime = 0.0f;
            }

            if (ScoreManager.RANK_NORMAL < resultScore) { grade = 1; rankName = "NORMAL"; }
            if (ScoreManager.RANK_BRONZE < resultScore) { grade = 2; rankName = "BRONZE"; }
            if (ScoreManager.RANK_SILVER < resultScore) { grade = 3; rankName = "SILVER"; }
            if (ScoreManager.RANK_GOLD < resultScore) { grade = 4; rankName = "GOLD"; }
        }
        else
        {
            gradeText.text = string.Format("Rank : {0}", rankName);
            gradeImage.sprite = gradeImageSource[grade];
            soundEffect.PlayOneShot(sound[1]);      //シンバル
            pushAnyButtonText.SetActive(true);      //Push Any Buttonを表示する
            isResultEnd = true;
        }
        scoreText.text = string.Format("Score : {0:0000000.00}", resultScore, ScoreManager.Instance.score);
    }

    //最終結果の表示
    private void Update()
    {
        //表示系統
        if (GameManager.Instance.isGameOver)
        {
            if (resultPanel.activeSelf == false) { ToActiveResultUI(); }
            if (isResultEnd == false) { ShowResult(); }
        }

        //何かしら入力があったら楽曲選択画面に戻る
        if (isResultEnd && InputManager.Instance.GetAnyKey())
        {
            GameManager.Instance.ReturnMusicSelect();
        }
    }
}
