﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 肉球のパーティクル
/// </summary>
public class NikukyuuParticle : SingletonMonoBehaviour<NikukyuuParticle>
{
    public ParticleSystem[] particle;

    public void PlayNikukyuuParticle(NoteLane lane)
    {
        int laneNumber = (int)lane;
        particle[laneNumber].Play();
    }

    public void StopNikukyuuParticle(NoteLane lane)
    {
        int laneNumber = (int)lane;
        particle[laneNumber].Stop();
    }
}
