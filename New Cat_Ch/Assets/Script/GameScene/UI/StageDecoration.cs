﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageDecoration : MonoBehaviour
{
    Transform myTransform;

    private float wave;
    private float rndTime;
    Vector3 rotation;
    private float rotationValue;
    public float range = 10.0f;
    bool dirction;

    private void Start()
    {
        myTransform = transform;
        rotation = myTransform.rotation.eulerAngles;
        rotationValue = Random.Range(0f, 360f);
        rndTime = Random.Range(0f, 180f);
        if (rotationValue > 180){ dirction = false; }
        else{ dirction = true; }
    }

    void Update()
    {
        if (dirction){ wave = Mathf.Sin(Time.time + rndTime) / range; }
        else{ wave = Mathf.Cos(Time.time + rndTime) / range; }
        myTransform.position += new Vector3(0, wave, 0);
        rotation.y = rotationValue;
        myTransform.rotation = Quaternion.Euler(rotation);
        rotationValue += 0.5f;
    }
}
