﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextFlow : MonoBehaviour
{
    //曲名をスクロール表示する

    public RectTransform parentBox;
    public GameObject textUI;
    public RectTransform textBox;
    [SerializeField] private Text musicTitleText;
    private float speed = 2.5f;
    private float initX;
    private float uiWidth;


    public void SetText(string setText)
    {
        musicTitleText.text = setText;
        musicTitleText = textUI.GetComponent<Text>();
        textBox.anchoredPosition = new Vector2(0, 0);

        //文字の幅にUIを合わせる
        textUI.GetComponent<RectTransform>().sizeDelta = new Vector2(musicTitleText.preferredWidth, textUI.GetComponent<RectTransform>().sizeDelta.y);
        uiWidth = textUI.GetComponent<RectTransform>().sizeDelta.x;     //幅変更後のタイトルTextの幅を取得

        initX = parentBox.sizeDelta.x / 2 + uiWidth / 2;                //表示範囲の端を表示のループ初期地点として取得

        Vector2 pos = textBox.anchoredPosition + new Vector2(initX, 0);
        textBox.anchoredPosition = pos;
    }

    void Update()
    {
        //タイトルTextをspeedに合わせて移動
        Vector2 pos = textBox.anchoredPosition - new Vector2(speed, 0);
        textBox.anchoredPosition = pos;

        //表示範囲外まで行ったら初期地点へ
        if (pos.x <= -initX)
        {
            textBox.anchoredPosition = new Vector2(initX, pos.y);
        }
    }
}
