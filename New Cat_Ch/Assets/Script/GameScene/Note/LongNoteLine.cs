﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//参考 : nn-hokuson.hatenablog.com/entry/2018/02/13/200114

//表示部分(Graphics)参考 : https://blog.applibot.co.jp/2017/07/24/tutorial-for-unity-3d/

public class LongNoteLine : MonoBehaviour
{
    public Mesh mesh;
    [SerializeField] private GameObject longStart = null;
    [SerializeField] private GameObject longEnd = null;
    [SerializeField] private Material longLineMat = null;
    private const float longNoteWidth = 1.4f;               //ロングノーツ幅

    //UV,ポリゴンを生成
    private void Start()
    {
        mesh = new Mesh();

        UpdateVertices();

        mesh.triangles = new int[]
        {
            0, 1, 2,
            1, 3, 2,
        };
    }

    //ロングノーツの描画
    void Update()
    {
        Vector3 stPos = longStart.transform.position;
        Vector3 enPos = longEnd.transform.position;

        UpdateVertices();
        Graphics.DrawMesh(mesh, Vector3.zero, Quaternion.identity, longLineMat, 0);
    }



    //描画するメッシュ位置を先端と後方ノーツの間に設定する
    void UpdateVertices()
    {
        Vector3 stPos = longStart.transform.position;
        Vector3 enPos = longEnd.transform.position;

        mesh.vertices = new Vector3[]
        {
            new Vector3(stPos.x - longNoteWidth/2, stPos.y, stPos.z),
            new Vector3(enPos.x - longNoteWidth/2, enPos.y, enPos.z),
            new Vector3(stPos.x + longNoteWidth/2, stPos.y, stPos.z),
            new Vector3(enPos.x + longNoteWidth/2, enPos.y, enPos.z),
        };

        mesh.RecalculateBounds();
    }

}
