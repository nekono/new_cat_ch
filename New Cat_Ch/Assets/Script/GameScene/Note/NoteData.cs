﻿
/// <summary>
/// ノーツのデータと種類
/// </summary>
public class NoteData
{
    public bool _active;
    public NoteLane _lane;
    public float _time;
    public NoteKind _kind;
    public float _length;
}

//ノーツの種類
public enum NoteKind
{
    NONE,
    NormalNote,
    LongNote
}

//レーン
public enum NoteLane
{
    Left,
    CenterLeft,
    CenterRight,
    Rigt
}
