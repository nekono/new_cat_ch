﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Note : MonoBehaviour
{
    [SerializeField] GameObject normalNote = null;
    [SerializeField] GameObject longNote = null;
    [SerializeField] GameObject longEnd = null;


    NoteData noteData = new NoteData();
    private InputManager.SetButtonList myLaneButton;
    private string myLaneButtonStr = "";

    public const float defaultOffsetZ = -10.0f;
    private float laneX = 0.0f;     //レーンのオブジェクトの位置
    private bool isActive = false;  //アクティブかどうか(パーティクルを残すための内部的なモノ)

    private bool isLongJudge;       //ロングノーツの先端の判定がミスではない際に最終ノーツに判定を移す

    private Transform thisTransform;
    private Transform normalTransform;
    private Transform longStTransform;
    private Transform longEnTransform;

    private MusicManager musicManager;

    private void Start()
    {
        thisTransform = transform;
        normalTransform = normalNote.transform;
        longStTransform = longNote.transform;
        longEnTransform = longEnd.transform;
        musicManager = MusicManager.Instance;
    }

    //ノーツのデータのセット
    public void SetNoteData(NoteLane _lane, float _time, NoteKind _kind, float _length)
    {
        noteData._lane = _lane;
        noteData._time = _time;
        noteData._kind = _kind;
        noteData._length = _length;
    }


    //レーンの設定
    public void SetLane()
    {
        switch (noteData._lane)
        {
            case NoteLane.Left:
                laneX = -3.8f;
                break;
            case NoteLane.CenterLeft:
                laneX = -1.27f;
                break;
            case NoteLane.CenterRight:
                laneX = 1.27f;
                break;
            case NoteLane.Rigt:
                laneX = 3.8f;
                break;

            default:
                break;
        }

        myLaneButton = (InputManager.SetButtonList)Enum.ToObject(typeof(InputManager.SetButtonList), noteData._lane + 1);
        myLaneButtonStr = myLaneButton.ToString();

        if (noteData._kind == NoteKind.NormalNote) { normalNote.SetActive(true); }
        if (noteData._kind == NoteKind.LongNote) {
            longNote.SetActive(true);
            longEnd.SetActive(true);
        }

        isActive = true;
    }

    //描画
    private void LateUpdate()
    {
        if (gameObject.activeSelf && isActive)
        {
            float z = defaultOffsetZ + (noteData._time - musicManager.playTime) * GameManager.Instance.noteSpeed;
            Vector3 pos = new Vector3(laneX, thisTransform.position.y, z);
            thisTransform.position = pos;

            //ロングノーツ
            if(noteData._kind == NoteKind.LongNote)
            {
                float endZ = -10.0f + (noteData._time - musicManager.playTime + noteData._length) * GameManager.Instance.noteSpeed;
                pos.z = endZ;
                longEnTransform.position = pos;
            }
        }
    }

    private void Update()
    {
        //判定
        if (gameObject.activeSelf && isActive)
        {
            NoteJudgeProcess();
            NoteMissJudge();
        }
    }


    private void NoteJudgeProcess()
    {
        //ノーツの判定順序管理を行うためにマネージャーに通知
        NotesManager.Instance.NoteOrderControl(noteData._lane, noteData._time);
        float judgeTime = Mathf.Abs(noteData._time - MusicManager.Instance.playTime);

        if (NotesManager.Instance.GetLaneNoteFirstTimes(noteData._lane) == noteData._time)
        {
            if (judgeTime <= ScoreManager.goodJudge + Time.deltaTime)
            {
                if ((InputManager.Instance.GetDownControllerKey(myLaneButtonStr) || GameManager.Instance.isAutoMode && judgeTime <= ScoreManager.parfectJudge) && isLongJudge == false)
                {
                    ScoreManager.Instance.JudgeScore(judgeTime);
                    if (noteData._kind == NoteKind.NormalNote) { SetActiveFalse(); }
                    if (noteData._kind == NoteKind.LongNote && isLongJudge == false) { SetActiveFalseCheckLong(); }
                }
            }


            //ロングノーツ先端を判定後、中盤と後端の判定
            if (isLongJudge)
            {
                judgeTime = Mathf.Abs(noteData._time + noteData._length - MusicManager.Instance.playTime);

                //ボタンを離したときの処理
                if (!InputManager.Instance.GetControllerKey(myLaneButtonStr) && GameManager.Instance.isAutoMode == false)
                {
                    NikukyuuParticle.Instance.StopNikukyuuParticle(noteData._lane);

                    if (judgeTime <= ScoreManager.goodJudge)
                    {
                        //後端のノーツの判定
                        ScoreManager.Instance.JudgeScore(judgeTime);
                        SetActiveFalseCheckLong();
                    }
                    else
                    {
                        //ロングノーツのボタン離しミス判定
                        SetActiveFalse();
                    }
                }
                //オートモード時のロングノーツの判定
                if (GameManager.Instance.isAutoMode && judgeTime <= ScoreManager.parfectJudge)
                {
                    NikukyuuParticle.Instance.StopNikukyuuParticle(noteData._lane);
                    ScoreManager.Instance.JudgeScore(judgeTime);
                    SetActiveFalseCheckLong();
                }
            }
        }
    }


    private void NoteMissJudge()
    {
        float missTime = noteData._time + ScoreManager.goodJudge;

        //以下取り逃しミス判定
        //NormalNoteの画面外一定距離で非アクティブ化
        if (noteData._kind == NoteKind.NormalNote && missTime < musicManager.playTime)
        {
            SetActiveFalse(false);
        }

        if (noteData._kind == NoteKind.LongNote && isActive)
        {
            //ロングノーツの先頭がまだ未判定
            if (missTime < musicManager.playTime && isLongJudge == false) { SetActiveFalse(false); }
            if (missTime + noteData._length < musicManager.playTime && isLongJudge) { SetActiveFalse(false); }
        }
    }


    private void SetActiveFalseCheckLong()
    {
        //ロングノーツ後端の判定
        if (isLongJudge) {
            if (isActive) { NotesManager.Instance.NoteJudged(noteData._lane, noteData._time); } //自身が判定されたことをマネージャーに伝える
            SetActiveFalse(false);
        }

        //ロングノーツ先端の判定
        if (isLongJudge == false && isActive)
        {
            isLongJudge = true;
            NikukyuuParticle.Instance.PlayNikukyuuParticle(noteData._lane);
        }
    }

    //有効なノーツとしてのすべての処理を終える
    private void SetActiveFalse(bool isPlayParticle = true)
    {
        if(noteData._kind == NoteKind.LongNote) { NikukyuuParticle.Instance.StopNikukyuuParticle(noteData._lane); }     //ロングノーツのパーティクル停止
        if (isActive) { NotesManager.Instance.NoteJudged(noteData._lane, noteData._time); }                             //自身が判定されたことをマネージャーに伝える
        isActive = false;
        isLongJudge = false;
        normalNote.SetActive(false);
        longEnd.SetActive(false);
        longNote.SetActive(false);
        if (isPlayParticle) { NormalNoteParticleManager.Instance.PlayParticle(myLaneButton); }
        gameObject.SetActive(false);
    }
}
