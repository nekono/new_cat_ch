﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBar : MonoBehaviour
{
    //画面上部のスコアの割合表示
    public Image image;
    public Color startColor;
    public Color endColor;

    void Update()
    {

        image.color = Color.Lerp(startColor, endColor, (float)ScoreManager.Instance.score / ScoreManager.MAX_SCORE);
        image.fillAmount = (float)ScoreManager.Instance.score / ScoreManager.MAX_SCORE;
    }

}
