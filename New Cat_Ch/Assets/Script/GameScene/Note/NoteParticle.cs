﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteParticle : MonoBehaviour
{
    public ParticleSystem particle;
    [SerializeField] private InputManager.SetButtonList myLane;

    //パーティクルの再生
    public void PlayParticle(InputManager.SetButtonList lane)
    {
        if (lane == myLane)
        {
            particle.Play();
        }
    }
}
