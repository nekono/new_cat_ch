﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicDataManager : SingletonMonoBehaviour<MusicDataManager>
{
    [SerializeField]
    private MusicList musicDataLists = null;


    //曲名から楽曲データ取得
    public MusicData SearchMusicData(string searchMusicName)
    {
        return musicDataLists.GetMusicLists().Find(musicName => musicName.GetMusicTitle() == searchMusicName);
    }

    //曲データベース
    public List<MusicData> GetMusicList()
    {
        return musicDataLists.GetMusicLists();
    }

    public int GetMusicListCount()
    {
        return musicDataLists.GetMusicLists().Count;
    }
}
