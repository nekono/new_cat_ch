﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


/// <summary>
/// 曲のデータ
/// </summary>

[Serializable]
[CreateAssetMenu(fileName = "MusicData", menuName = "CreateMusicData")]

public class MusicData : ScriptableObject
{
    //曲名
    [SerializeField] private string musicTitle = "unknown";

    //楽曲
    [SerializeField] private AudioClip musicClip = null;

    //ジャケット絵
    [SerializeField] private Sprite jacket = null;

    //譜面csv
    [SerializeField] private TextAsset[] noteData = null;

    //作曲者
    [SerializeField] private string artist = "unknown";

    //ジャケット制作者
    [SerializeField] private string illustrator = "unknown";

    //譜面制作者
    [SerializeField] private string noteDesignerName = "unknown";

    //難易度レベル
    [SerializeField] private string[] noteDifficulty = null;

    //曲の短ループ
    [SerializeField] private AudioClip shortClip = null;


    public string GetMusicTitle() { return musicTitle; }
    public AudioClip GetMusicClip() { return musicClip; }
    public Sprite GetJacket() { return jacket; }
    public TextAsset GetNoteData(int difficulty) { return noteData[difficulty]; }
    public string GetArtist() { return artist; }
    public string GetIllustrator() { return illustrator; }
    public string GetNoteDesigner() { return noteDesignerName; }
    public string GetNoteDifficulty(int difficulty) { return noteDifficulty[difficulty]; }
    public AudioClip GetShortClip() { return shortClip; }
}
