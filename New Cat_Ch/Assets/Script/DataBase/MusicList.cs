﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//曲のデータリスト
[CreateAssetMenu(fileName = "MusicList", menuName = "CreateMusicList")]

public class MusicList : ScriptableObject
{
    [SerializeField]
    private List<MusicData> musicLists = new List<MusicData>();

    public List<MusicData> GetMusicLists()
    {
        return musicLists;
    }
}
