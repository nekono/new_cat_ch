﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicInfoData : MonoBehaviour
{
    [SerializeField] private string musicName { get; set; }       //曲名
    [SerializeField] private int diffculty { get; set; }          //難易度(0,1,2)
    [SerializeField] private float noteSpeed { get; set; }        //ノーツスピード
    [SerializeField] private bool playAutoMode { get; set; }      //オートモード
}
