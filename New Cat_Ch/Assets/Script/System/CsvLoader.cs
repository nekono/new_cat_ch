﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Linq;
using CatCh.System;

/// <summary>
/// CSVのテキストからNoteのリストを生成
/// </summary>
namespace CatCh.System
{
    public static class CsvLoader
    {
        public static List<NoteData> CsvToNoteList(TextAsset noteData)
        {
            StringReader reader = new StringReader(noteData.text);
            List<NoteData> csvDatas = new List<NoteData>();             //csvから取り出したデータ
            int noteValue = 0;

            while (reader.Peek() != -1)
            {
                string[] line = reader.ReadLine().Split(',');
                NoteData data = new NoteData();
                data._active = false;                                                               //ノーツ出現済みかどうか
                data._lane = (NoteLane)Enum.ToObject(typeof(NoteLane), int.Parse(line[0]) - 1);     //レーン
                data._time = float.Parse(line[1]) / 1000.0f;                                        //時間を前作の基準からUnity基準に変換
                data._kind = (NoteKind)Enum.ToObject(typeof(NoteKind), int.Parse(line[2]));         //ノーツ種類
                                                                                                    //line[3]は今リメイク作では不使用
                if (line.Length > 4) { data._length = float.Parse(line[4]) / 1000.0f; }             //ロングノーツの長さ
                csvDatas.Add(data);

                if (data._kind == NoteKind.NormalNote) { noteValue++; }
                if (data._kind == NoteKind.LongNote) { noteValue += 2; }    //ロングノーツは2個分カウント
            }

            ScoreManager.Instance.SetNotesValue(noteValue);         //スコアマネージャーに総ノーツ数を渡す
            csvDatas.Sort((a, b) => a._time.CompareTo(b._time));
            return csvDatas;
        }
    }
}