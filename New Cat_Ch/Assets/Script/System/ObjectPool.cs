﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CatCh.System
{
    public class ObjectPool<T> where T : MonoBehaviour
    {
        private Transform poolParent = null;
        private List<T> objectPool = null;      //オブジェクトプール
        private T createObj = null;             //生成物


        //オブジェクトプール生成
        public void CreatePool(T obj, string poolName)
        {
            poolParent = new GameObject(poolName).transform;
            createObj = obj;
            objectPool = new List<T>();
        }

        //プーリング
        public T CreateObject()
        {
            foreach (var poolObj in objectPool)
            {
                if (poolObj.gameObject.activeSelf == false)
                {
                    poolObj.gameObject.SetActive(true);
                    return poolObj;
                }
            }

            T newObj = Object.Instantiate(createObj, poolParent);
            newObj.gameObject.SetActive(true);
            objectPool.Add(newObj);
            return newObj;
        }
    }
}
