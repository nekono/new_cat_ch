﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using CatCh.System;

//選曲画面からのシーン遷移"以外"のシーン遷移スクリプト
namespace CatCh.System
{
    public class SceneLoader : SingletonMonoBehaviour<SceneLoader>
    {
        [SerializeField] private Image loadingImage = null;           //シーン切替時表示Image
        private bool isLoadingProcess = false;                        //シーン切替処理稼働中か

        private AsyncOperation loadAsync;

        //ゲームシーンを読み込む
        public void LoadScene(SceneName sceneName)
        {
            if (isLoadingProcess == false)
            {
                isLoadingProcess = true;
                DontDestroyOnLoad(gameObject);

                loadAsync = SceneManager.LoadSceneAsync((int)sceneName);
                loadAsync.allowSceneActivation = false;
                StartCoroutine("FadeOut");
            }
        }

        IEnumerator FadeOut()
        {
            float alpha = 0.0f;
            while (alpha < 1.0f)
            {
                alpha += 0.1f;
                loadingImage.color = new Color(1.0f, 1.0f, 1.0f, alpha);
                yield return new WaitForSeconds(0.05f);
            }
            yield return new WaitForSeconds(0.5f);
            while (true)
            {
                yield return null;
                if (loadAsync.progress >= 0.9f)
                {
                    loadAsync.allowSceneActivation = true;
                    StartCoroutine("FadeIn");
                    break;
                }
            }
        }

        IEnumerator FadeIn()
        {
            float alpha = 1.0f;
            while (alpha > 0.0f)
            {
                alpha -= 0.1f;
                loadingImage.color = new Color(1.0f, 1.0f, 1.0f, alpha);
                yield return new WaitForSeconds(0.05f);
            }
            Destroy(gameObject);
        }
    }
}